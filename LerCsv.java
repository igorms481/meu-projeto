package projeto.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class LerCsv {

	public static void main(String[] args) throws IOException{
		
		//ENDEREÇOS DAS PASTAS
		String url_pendentes = "/home/administrador/Documents/PENDENTES";
		String url_invalidado = "/home/administrador/Documents/INVALIDADO";
		String url_validado = "/home/administrador/Documents/VALIDADO";
		
		//ENDEREÇO DA PASTA PENDENTES
		File pasta = new File(url_pendentes);
		
		//ADICIONA EM UM ARRAY TODAS OS ARQUIVOS DA PASTA
        File[] lista_arquivos = pasta.listFiles();
        
        //ACESSAR 1 ARQUIVO POR VEZ
        for(File arquivo:lista_arquivos) {
        	Boolean validado = true;
        	System.out.println(arquivo.getName());
        	
        	//LER O ARQUIVO DA VEZ
        	BufferedReader csv = new BufferedReader(new FileReader(arquivo));
        	String sc;
			while((sc = csv.readLine()) != null) {
				
				//TRATA O ERRO SE TIVER UMA LINHA VAZIA
				if(sc.trim().isEmpty()) {
        			continue;
        		}
            	
				//SE TIVER ALGUM ARQUIVO CSV ERRADO, ENVIA PARA PASTA INVALIDADO
				String[] dados_separados = sc.split(";");
            	if(dados_separados.length != 4) {
            		arquivo.renameTo(new File(url_invalidado, arquivo.getName()));
            		System.out.println("Arquivo movido para INVALIDADO");
            		validado = false;
            		break;
            	}
        	
        	}
			//SE CHEGOU AQUI É PORQUE O ARQUIVO CSV ESTÁ CORRETO E VAI SER ENVIADO PARA PASTA VALIDADO
        	if(validado == true) {
        		arquivo.renameTo(new File(url_validado, arquivo.getName()));
            	System.out.println("Arquivo movido para VALIDADO");
        	}	
        	
        }

	}

}
